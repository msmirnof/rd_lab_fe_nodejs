const express = require('express');
const app = express();
const router = require('./router');

const PORT = 8080;

function logger(req, res, next) {
  console.log(`Request: ${req.method}, ${req.originalUrl}`);
  res.on('finish', () => {
    console.info(
      `${res.statusCode} ${res.statusMessage}; ${
        res.get('Content-Length') || 0
      }b sent`
    );
  });
  next();
}
app.use(express.json());
app.use(logger);
app.use('/api', router);

app.listen(PORT, () => {
  console.log(`The server has been started at ${PORT} port`);
});
