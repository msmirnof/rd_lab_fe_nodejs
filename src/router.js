const express = require('express');
const router = express.Router();
const service = require('./service');

router.get('/files', async (req, res) => {
  try {
    const fileNames = await service.getFileNames();
    res.status(200).json({
      message: 'Success',
      files: fileNames
    });
  } catch (err) {
    res.status(500).json({
      message: 'Server error'
    });
  }
});

router.get('/files/:fileName', async (req, res) => {
  const { fileName } = req.params;
  try {
    const exist = await service.checkExistFile(fileName);
    const {
      filename,
      extension,
      content,
      uploadedDate
    } = await service.fileInfo(fileName);
    if (exist) {
      res.status(200).json({
        message: 'Success',
        filename: filename,
        content: content,
        extension: extension,
        uploadedDate: uploadedDate
      });
    } else {
      res.status(400).json({
        message: `No file with '${fileName}' filename found`
      });
    }
  } catch (err) {
    res.status(500).json({
      message: 'Server error'
    });
  }
});

router.post('/files', async (req, res) => {
  try {
    const { filename, content } = req.body;
    if (filename && content) {
      await service.createFile(filename, content);
      res.status(200).json({
        message: 'File created successfully'
      });
    } else {
      res.status(400).json({
        message: `Please specify 'content' parameter`
      });
    }
  } catch (err) {
    if (err.message === 'File already exist') {
      res.status(400).json({
        message: err.message
      });
    }
    res.status(500).json({
      message: 'Server error'
    });
  }
});

module.exports = router;
