const { constants } = require('fs');
const fs = require('fs/promises');
const path = require('path');

const filePath = path.resolve(__dirname, 'files');

async function getFileNames() {
  return await fs.readdir(filePath);
}

async function createFile(fileName, content) {
  const exist = await checkExistFile(fileName);
  if (exist) {
    throw new Error('File already exist');
  }
  try {
    await fs.writeFile(path.resolve(filePath, fileName), content);
  } catch (err) {
    return new Error('Failed to create file');
  }
}

async function checkExistFile(fileName) {
  try {
    await fs.access(path.resolve(filePath, fileName), constants.R_OK);
    return true;
  } catch (err) {
    return false;
  }
}

async function readFile(fileName) {
  try {
    return await fs.readFile(
      path.resolve(filePath, fileName),
      'utf-8',
      (err, content) => {
        if (err) {
          throw new Error(`No file with ${fileName} filename found`);
        }
        return content;
      }
    );
  } catch (err) {
    return new Error('Failed to read file');
  }
}

async function fileInfo(fileName) {
  try {
    const content = await readFile(fileName);
    if (!content) {
      throw new Error(`No file with ${fileName} filename found`);
    }
    return {
      filename: fileName,
      content: content,
      extension: path.extname(fileName).replace('.', ''),
      uploadedDate: '2021-07-07T14:14:17.594Z'
    };
  } catch (err) {
    return new Error('Failed to read file');
  }
}

module.exports = {
  getFileNames,
  createFile,
  fileInfo,
  checkExistFile
};
